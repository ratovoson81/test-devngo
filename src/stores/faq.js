import { ref } from "vue";
import { defineStore } from "pinia";

export const useFaqStore = defineStore("faq", () => {
  const faq = ref([
    {
      id: 1,
      title: "Comment créer un compte utilisateur ?",
      answer:
        "Pour créer un compte utilisateur, rendez-vous sur la page d'inscription, remplissez le formulaire avec vos informations personnelles, et cliquez sur le bouton 'S'inscrire'. Un e-mail de confirmation vous sera envoyé pour activer votre compte.",
    },
    {
      id: 2,
      title: "Comment réinitialiser mon mot de passe ?",
      answer:
        "Si vous avez oublié votre mot de passe, cliquez sur 'Mot de passe oublié' sur la page de connexion. Entrez votre adresse e-mail associée à votre compte, suivez les instructions envoyées par e-mail pour réinitialiser votre mot de passe.",
    },
    {
      id: 3,
      title: "Puis-je changer mon nom d'utilisateur ?",
      answer:
        "Malheureusement, vous ne pouvez pas changer votre nom d'utilisateur une fois qu'il est créé. Choisissez soigneusement lors de l'inscription, car il sera lié à votre compte de manière permanente.",
    },
    {
      id: 4,
      title: "Comment contacter le service client ?",
      answer:
        "Pour contacter notre service client, vous pouvez envoyer un e-mail à support@exemple.com ou utiliser le formulaire de contact sur notre site Web. Nous répondrons à votre demande dans les 24 heures.",
    },
    {
      id: 5,
      title: "Quels modes de paiement acceptez-vous ?",
      answer:
        "Nous acceptons les paiements par carte de crédit (Visa, MasterCard), PayPal et virement bancaire. Assurez-vous de choisir l'option de paiement qui vous convient le mieux lors de la finalisation de votre commande.",
    },
  ]);

  const postFaq = (param) => {
    const lastId = Math.max(...faq.value.map((item) => item.id), 0);

    faq.value.push({
      id: lastId + 1,
      title: param,
      answer: "",
    });
  };

  const updateAnswerById = (id, newAnswer) => {
    const item = faq.value.find((item) => item.id === id);
    if (item) {
      item.answer = newAnswer;
    }
  };

  const deleteItemById = (id) => {
    const index = faq.value.findIndex((item) => item.id === id);
    if (index !== -1) {
      faq.value.splice(index, 1);
    }
  };

  return { faq, postFaq, updateAnswerById, deleteItemById };
});
