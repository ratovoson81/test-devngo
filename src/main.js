import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { createPinia } from "pinia";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.js";
import VueCollapsiblePanel from "@dafcoe/vue-collapsible-panel";
import "vue3-toastify/dist/index.css";
import "bootstrap-icons/font/bootstrap-icons.css";

const app = createApp(App);

app.use(createPinia());

app.use(VueCollapsiblePanel);
app.use(router);

app.mount("#app");
